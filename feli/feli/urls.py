from django.conf.urls import patterns, include, url
from django.contrib import admin
from answerdiff import views
from frontend.views import main
from django_cas_ng import views as cas

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', cas.login, name='cas_login'),
    url(r'^accounts/logout/$', cas.logout, name='cas_logout'),
    url(r'^api/', include('answerdiff.urls')),
    url(r'^gordian-knot/', main),
    url(r'^', include('testme.urls')),
]
