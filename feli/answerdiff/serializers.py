from rest_framework import serializers
from .models import Question, Submission, Profile, Comment, Contest, ClarificationMessages

class QuestionSerializer(serializers.ModelSerializer):

    level = serializers.IntegerField(source='question_level')
    number = serializers.IntegerField(source='question_number')
    title = serializers.CharField(source='question_title')
    # desc = serializers.TextField(source='question_desc') #========Not working, don't know why. :P
    image = serializers.ImageField(source='question_image')
    upload_type = serializers.CharField(source='question_upload_type')
    is_mathjax_enabled = serializers.BooleanField(source='question_enable_mathjax')

    class Meta:
        model = Question
        fields = ('level', 'number', 'title', 'image', 'is_mathjax_enabled', 'upload_type')

class SubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submission
        fields = ('id', 'submission_string')

class ScoreboardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('user_nick' ,'user_score', 'user_country')

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('comment_message',)

class SubmissionSerializerforMySubmission(serializers.ModelSerializer):
    class Meta:
        model = Submission
        fields = ('submission_user', 'submission_question', 'submission_question', 'submission_state')
        depth = 2

class ContestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contest
        fields = ('contest_name', 'contest_description', 'contest_rules', 'contest_start_time', 'contest_end_time')

class ClarificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClarificationMessages
        fields = '__all__'

class QuestionCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'
