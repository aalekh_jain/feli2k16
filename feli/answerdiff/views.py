import datetime

from django.contrib.auth.models import User

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated

from .models import Question, Submission, Comment, Contest,\
                    AcceptedQuestion, Profile, ClarificationMessages

from .serializers import QuestionSerializer, SubmissionSerializer,\
                         ScoreboardSerializer, CommentSerializer,\
                         SubmissionSerializerforMySubmission, ContestSerializer,\
                         ClarificationSerializer, QuestionCommentSerializer

from .task import checker_queue

FILE = "FL"
STRING = "ST"
# SUBMISSION_STATE_CHOICES = { 'WA': 'Wrong Answer', 'AC': 'Accepted', 'PR': 'Processing', 'NA': 'Not Attempted' }
SUBMISSION_STATE_CHOICES = { 'WA': 'WA', 'AC': 'AC', 'PR': 'PR', 'NA': 'NA' } #Keep it simple for now
MAX_COMMENT_LENGTH = 255
GLOBAL_TIME_LIMIT = datetime.timedelta(0,30)

def is_level_accessible(profile, level):
    return int(level) <= profile.user_access_level


class UTC(datetime.tzinfo):
    def utcoffset(self, dt):
        return datetime.timedelta(0)
    def tzname(self, dt):
        return "UTC"
    def dst(self, dt):
        return datetime.timedelta(0)

utc = UTC()

class ProblemsClass(APIView):

    """
    List all question which a user can access, admin can view all the problems.
    Use it like this ->
        url(r'^problems/$', views.ProblemsClass.as_view()),
    """

    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    # renderer_classes = (JSONRenderer, )   # uncomment this to send "only" json response

    def get(self, request, format=None):

        user = User.objects.get(username=request.user.username)
        profile = user.profile
        problem_details = []    # This will contain the final list of problems to send

        # Return all questions if staff, else return only accessible questions
        if request.user.is_staff:
            questions = Question.objects.all().order_by("question_level").order_by("question_number")
        else:
            questions = Question.objects.filter(question_level__lte=profile.user_access_level).order_by("question_level").order_by("question_number")

        all_submissions = Submission.objects.filter(submission_user__username=user.username)

        # level_dict = {'question_level': 0, 'question_list': []} # This is how level dict will look like
        for question in questions:
            accepted = all_submissions.filter(submission_question=question).filter(submission_state="AC")
            wrong_answer = all_submissions.filter(submission_question=question).filter(submission_state="WA")
            processing = all_submissions.filter(submission_question=question).filter(submission_state="PR")

            if accepted:
                current_status = SUBMISSION_STATE_CHOICES["AC"]
            elif wrong_answer:
                current_status = SUBMISSION_STATE_CHOICES["WA"]
            elif processing:
                current_status = SUBMISSION_STATE_CHOICES["PR"]
            else:
                current_status = SUBMISSION_STATE_CHOICES["NA"]

            level_index = int(question.question_level) - 1
            question_info = {
                "number" : question.question_number,
                "title" : question.question_title,
                "status" : current_status
            }

            try:                        # Try if the index corresponding to level_index exists or not
                problem_details[level_index]["question_data"].append(question_info)
            except IndexError, e:       # Insert new level if not present yet
                problem_details.append({
                    "question_level": int(question.question_level),
                    "question_data": [question_info]
                })

        context = {
            "user_nick" : profile.user_nick,
            "problem_details" :problem_details
        }
        return Response(context)

class LevelDetails(APIView):

    """
    List all the questions of a level (along with their submission status), admin can access all the levels
    How to use ->
            url(r'^level/(?P<level>[0-9]+)/$', views.LevelDetails.as_view()),
    """

    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def get_object(self, level):
        try:
            return Question.objects.filter(question_level=level).order_by("question_number")
        except Question.DoesNotExist:
            context = {
                "error": "This level does not exist"
            }
            return Response(context, status=status.HTTP_404_NOT_FOUND)

    def get(self, request, level, format=None):
        user = User.objects.get(username=request.user.username)
        profile = user.profile

        if request.user.is_staff:
            questions = self.get_object(level)
        else:
            if is_level_accessible(profile, level):
                questions = self.get_object(level)
            else:
                content = {
                    "user_nick":profile.user_nick,
                    "error": "You are not authorized for this"
                }
                return Response(content, status=status.HTTP_406_NOT_ACCEPTABLE)

        all_submissions = Submission.objects.filter(submission_user__username=user.username)
        problem_details = []

        for question in questions:
            accepted = all_submissions.filter(submission_question=question).filter(submission_state="AC")
            wrong_answer = all_submissions.filter(submission_question=question).filter(submission_state="WA")
            processing = all_submissions.filter(submission_question=question).filter(submission_state="PR")

            if accepted:
                current_status = SUBMISSION_STATE_CHOICES["AC"]
            elif wrong_answer:
                current_status = SUBMISSION_STATE_CHOICES["WA"]
            elif processing:
                current_status = SUBMISSION_STATE_CHOICES["PR"]
            else:
                current_status = SUBMISSION_STATE_CHOICES["NA"]

            level_index = 0 # Why 0.??, Because only information of only a single level is being sent.
            question_info = {
                "number" : question.question_number,
                "title" : question.question_title,
                "status" : current_status
            }

            try:        # Try if the index corresponding to level_index exists or not
                problem_details[level_index]["question_data"].append(question_info)
            except IndexError, e:    # Insert new level if not present yet
                problem_details.append({
                    "question_level": question.question_level,
                    "question_data": [question_info]
                })

        context = {
            "user_nick" : profile.user_nick,
            "problem_details" : problem_details
        }
        return Response(context)


class QuestionDetails(APIView):
    """
    Show the question corresponding to level and number.
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    # renderer_classes = (JSONRenderer, )  # uncomment this to send "only" json response

    def get_object(self, question_level, question_number):
        try:
            return Question.objects.filter(question_level = question_level).filter(question_number = question_number )
        except Question.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get_contest_type(self):
        contest_type = Contest.objects.all().first().contest_user_level_increment_type
        return str(contest_type)

    def get_question_type(self):
        contest_upload_type = Contest.objects.all().first().contest_question_type
        return str(contest_upload_type)

    # def get_contest_consecutive_submission_halt_time(self):
    #     contest_consecutive_submission_halt_time = Contest.objects.all().first().contest_consecutive_submission_halt_time
    #     return contest_consecutive_submission_halt_time

    def get(self, request, question_level, question_number, format=None):
        question = self.get_object(question_level, question_number)
        user = User.objects.get(username=request.user.username)
        profile = user.profile

        if len(question):
            question_comments = Comment.objects.filter(comment_question=question).filter(comment_is_approved=True).order_by("comment_timestamp")

            if is_level_accessible(profile, question_level) or request.user.is_staff :
                question_details = question[0] # ========== Doubt, check this again
                serializer = QuestionSerializer(question_details)

                comments = []
                for i in question_comments: #Serialize every comment and send
                    comments.append(QuestionCommentSerializer(i).data)
                context = {
                    "question_data" : serializer.data,
                    "user_nick" : profile.user_nick,
                    "question_comments" : comments
                }
                return Response(context)
            else:
                content = {
                    "user_nick" : profile.user_nick,
                    "error" : "You don't have appropriate access"
                }
                return Response(content, status=status.HTTP_406_NOT_ACCEPTABLE)

        else:
            content = {
                "user_nick" : profile.user_nick,
                "error" : "Question Not Available",
            }
            return Response(content, status=status.HTTP_404_NOT_FOUND)

class SubmitAnswer(APIView):

    """
    Use the post Function of this class to submit the solution of the respective questions.
    How to use ->
        url(r'^submit/(?P<question_level>[0-9]+)/(?P<question_number>[0-9]+)/$', views.SubmitAnswer.as_view()),
    """

    def get_object(self, question_level, question_number):
        try:
            return Question.objects.filter(question_level = question_level).filter(question_number = question_number )
        except Question.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get_contest_type(self):
        contest_type = Contest.objects.all().first().contest_user_level_increment_type
        return str(contest_type)

    def get_question_type(self):
        contest_upload_type = Contest.objects.all().first().contest_question_type
        return str(contest_upload_type)

    def post(self, request, question_level, question_number, format=None):
        """
        Function to submit solution
        """

        user = User.objects.get(username=request.user.username)
        profile = user.profile
        question = Question.objects.filter(question_level=question_level).filter(question_number=question_number)

        if not is_level_accessible(profile, question_level):
            content = {
                "user_nick" : user.profile.user_nick,
                "error" : "You don't have the access to submit the solution of this question."
            }
            return Response(content, status=status.HTTP_406_NOT_ACCEPTABLE)

        # Check for resubmission
        check_resubmission = AcceptedQuestion.objects.filter(record_user=request.user).filter(record_question=question).filter(accepted=True)
        if len(check_resubmission):
            content = {
                "user_nick" : profile.user_nick,
                "error" : 'Already Accepted'
            }
            return Response(content, status=status.HTTP_406_NOT_ACCEPTABLE)

        time_last = None
        time_last_query = Submission.objects.filter(submission_user__username=request.user.username).filter(submission_question__question_level=question_level).filter(submission_question__question_number=question_number).filter(submission_state="WA").order_by("submission_timestamp").last()

        if time_last_query:
            time_last = time_last_query.submission_timestamp
        time_limit = GLOBAL_TIME_LIMIT

        # Prevent user to spam, add delay before consecutive submissions
        if(time_last is None or time_last + time_limit <= datetime.datetime.now(utc)):
            type_of_contest =  self.get_contest_type()
            type_of_submission = self.get_question_type()
            if type_of_submission != STRING:
                content = {
                    "user_nick" : user.profile.user_nick,
                    "error" : "WRONG TYPE SUBMISSION",
                }
                return Response(content, status=status.HTTP_405_METHOD_NOT_ALLOWED)

            serializer = SubmissionSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save(
                    submission_user=request.user,
                    submission_question=self.get_object(question_level,question_number)[0],
                )
                checker_queue.delay(int(serializer.data['id']))
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            content = {
                "user_nick": profile.user_nick,
                "error": "Please try after some time",
            }
            return Response(content, status=status.HTTP_406_NOT_ACCEPTABLE)

class Scoreboard(APIView):
    """
    Class for displaying score board,
    its better if this is used along with cron and the scoreboard is fetched from a static file
    How to use ->
            url(r'^generate_scoreboard/$',views.Scoreboard.as_view()),
    """

    def get(self, request, format=None):
        scoreboard = Profile.objects.all().order_by("-user_score")
        # If you want to remove admin in scoreboard, then remove it on frontend. "NO" extra computation here
        serializer = ScoreboardSerializer(scoreboard, many=True)
        return Response(serializer.data)

class PostComments(APIView):
    """
    Class for posting comments
    """

    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    #def get_contest_consecutive_submission_halt_time(self):
    #    contest_consecutive_submission_halt_time = Contest.objects.all().first().contest_consecutive_submission_halt_time
    #    return contest_consecutive_submission_halt_time

    def post(self, request, question_level, question_number, format=None):
        """
        Use this Function to submit comments.
        How to use ->
            url(r'^comment/(?P<question_level>[0-9]+)/(?P<question_number>[0-9]+)/$',views.PostComments.as_view()),
        """

        user = User.objects.get(username=request.user.username)
        profile = user.profile
        question = Question.objects.filter(question_level=question_level).filter(question_number=question_number)

        if not is_level_accessible(profile, question_level):
            content = {
                "user_nick" : user.profile.user_nick,
                "error" : "You can't comment on this question.",
            }
            return Response(content, status=status.HTTP_406_NOT_ACCEPTABLE)

        time_last = None
        time_last_query = Comment.objects.filter(comment_user__username=request.user.username).order_by("comment_timestamp").last()
        if time_last_query:
            time_last = time_last_query.comment_timestamp
        time_limit = GLOBAL_TIME_LIMIT

        if(time_last is None or time_last + time_limit <= datetime.datetime.now(utc)):

            serializer = CommentSerializer(data=request.data)
            if not request.data["comment_message"] or len(str(request.data["comment_message"])) >= MAX_COMMENT_LENGTH:
                content = {
                    "user_nick" : profile.user_nick,
                    "error" : "Try to submit a shorter comment."
                }
                return Response(content, status=status.HTTP_406_NOT_ACCEPTABLE)
            if serializer.is_valid():
                serializer.save(
                    comment_user = request.user,
                    comment_question = question[0],
                )
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            content = {
                "user_nick" : user.profile.user_nick,
                "error": 'Please try to comment after some time'
            }
            return Response(content, status=status.HTTP_406_NOT_ACCEPTABLE)

class MySubmissions(APIView):
    """
    List all Submissions of logged in user of return all submissions if user is admin
    """

    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    # renderer_classes = (JSONRenderer, )  # uncomment this to send "only" json response
    def get(self, request, format=None):
        user = User.objects.get(username = request.user.username)
        profile = user.profile
        if request.user.is_staff:
            submissions = Submission.objects.all().order_by("submission_timestamp")
        else:
            submissions = Submission.objects.filter(submission_user=user).order_by("submission_timestamp")
        serializer = SubmissionSerializerforMySubmission(submissions, many=True)
        return Response(serializer.data)



class Rejudge(APIView):
    """
    Rejudge the question corresponding to level and number

    """
    def rejudge(self, question):
        submissions = Submission.objects.filter(submission_question=question)
        for submission in submissions:
            # If previously accepted, then score down 0
            # tries down
            user = submission.submission_user
            profile = user.profile
            question = submission.submission_question

            records = AcceptedQuestion.objects.filter(record_user=user).filter(record_question=question)
            if len(records)==0:
                continue

            record = records[0]

            if record.accepted:
                # print profile.user_score
                profile.user_score = profile.user_score - record.score
                profile.save()
                record.accepted = False
                record.score = 0.0
            record.tries = 1 # =========== Check if it's not going to record.tries-=1
            record.save()

            submission.submission_score = 0.0
            submission.submission_state = SUBMISSION_STATE_CHOICES["PR"]
            submission.save()

            checker_queue.delay(submission.id, True)

    def get(self, request, question_level, question_number, format=None):
        question = Question.objects.filter(question_level=question_level).filter(question_number=question_number)
        user = User.objects.get(username = request.user.username)
        profile = user.profile

        if request.user.is_staff:
            self.rejudge(question)
            content = {
                "user_nick" : profile.user_nick,
                "rejudge_status" : "Rejudging completed",
            }
            return Response(content)

        else:
            content = {
                "user_nick" : profile.user_nick,
                "error" : "Nothing to see here"
            }
            return Response(content, status=status.HTTP_406_NOT_ACCEPTABLE)

class ContestDetails(APIView):
    """
    Send Contest details
    How to use ->
        url(r'^contest/$',views.ContestDetails.as_view()),
    """
    def get(self, request, format=None):
        user = User.objects.get(username=request.user.username)
        profile = user.profile
        contest = Contest.objects.all().first()
        serializer = ContestSerializer(contest, many=True)
        return Response(serializer.data)

class ClarificationDetails(APIView):
    """
    Send Clarifications during the contest, could be better if this can also be done using cron
    How to use ->
        url(r'^clarification/$',views.ClarificationDetails.as_view()),
    """
    def get(self, request, format=None):
        clarifications = ClarificationMessages.objects.all()
        serializer = ClarificationSerializer(clarifications, many=True)
        return Response(serializer.data)
