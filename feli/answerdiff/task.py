
from __future__ import absolute_import

import commands
import imp
import os

from celery import shared_task
from feli.celery import app

from .models import Submission, AcceptedQuestion, Contest

import logging
logger = logging.getLogger(__name__)

# =============== Here are base score and NO_OF_RETRIES_BEFORE_DECREASING_POINTS that depends on the contest.
# =============== For example, in gordian knot BASE_SCORE was used as 32 and NO_OF_RETRIES_BEFORE_DECREASING_POINTS was 10
# =============== IN "GENERAL" Keep BASE_SCORE as 0 in other contests.
BASE_SCORE = 32
NO_OF_RETRIES_BEFORE_DECREASING_POINTS = 10 # Score will only decrease if question.question_reduce_ratio is not set to 1

@shared_task
def test(param):
    return "The test tak executed with argument '%s' " % param


@app.task()
def checker_queue(submission_id, rechecking=False):
    try:
        submission = Submission.objects.get(id=submission_id)
    except: # TODO See the error that comes up and update it here
        return 1

    user = submission.submission_user
    # logger.info("User is " + str(user))
    profile = user.profile
    # logger.info("Profile is " + str(profile))
    question = submission.submission_question
    # logger.info("Question is " + str(question))
    input_type = question.question_upload_type
    records = AcceptedQuestion.objects.filter(record_user=user).filter(record_question=question)
    try:
        record = records[0]
        # logger.info(record)
        # logger.info("Accepted or not: " + str(record.accepted))
        if record.accepted:
            # logger.info("After rejudging also, the question is accepted, therefore returning")
            return 1
    except IndexError:
        record = AcceptedQuestion(record_user=user, record_question=question)

    if not rechecking:
        record.tries += 1
    else:
        record.tries = 1

    ans_correct = False

    if input_type == "ST":
        ans = submission.__check_ans__()
        # logger.info("Hi:" + ans + str(submission.id))
        ans_correct = ans == "AC"

    # TODO Else for file, update submission in that else

    record.accepted = ans_correct
    if record.accepted:
        record.score = BASE_SCORE + int (question.question_points * (question.question_reduce_ratio ** int((record.tries - 1)/NO_OF_RETRIES_BEFORE_DECREASING_POINTS) ))
        # logger.info("User got the score " + str(record.score) + " in rejudging")
        record.accepted = True
        # logger.info("Profile score previously was " + str(profile.user_score))
        profile.user_score = profile.user_score + record.score
        submission.submission_score = record.score
        submission.save()
    record.save()


    contest = Contest.objects.all().first()
    user_records = AcceptedQuestion.objects.filter(record_user=user).filter(accepted=True)


    if contest.contest_user_level_increment_type == "TYPE1":
        profile.user_access_level = max(profile.user_access_level,
                                        len(user_records)/contest.contest_number_to_increment_level_at)

    elif contest.contest_user_level_increment_type == "TYPE2":
        accepted_level = [0 for i in xrange(1, profile.user_access_level+1)]
        for user_record in user_records:
            logger.info(user_record.record_question.question_level)
            accepted_level[user_record.record_question.question_level-1] += 1

        flag = True
        for i in accepted_level:
            if i < contest.contest_number_to_increment_level_at:
                flag = False
                break

        if flag:
            profile.user_access_level += 1


    profile.save()  # Level Increase
    return 0
