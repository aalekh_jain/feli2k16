"""
    Custom authentication backend to populate database with user data.
"""


from django_cas.backends import CASBackend
from models import Profile

import requests

class PopulatedCASBackend(CASBackend):
    """
        CAS authentication backend with user data populated from AD
    """

    def __init__(self):
        super(PopulatedCASBackend, self).__init__()

    def authenticate(self, ticket, service, request):
        """
            Authenticates CAS ticket and retrieves user data
        """

        user = super(PopulatedCASBackend, self).authenticate(ticket, service, request)
        attributes = request.session['attr'] or None
        request.session['attr'] = None
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip_address = x_forwarded_for.split(',')[0]
        else:
            ip_address = request.META.get('REMOTE_ADDR')

        r = requests.post('https://felicity.iiit.ac.in/auth/get_magic_user/', data={'oauth_id': str(user)})
        if r.status_code == 403:
            return None

        data = r.json()

        profile, created = Profile.objects.get_or_create(user=user)
        profile.user_nick = data.get('nick')
        profile.user_country = data.get('country')

        profile.save()
        return user
