from django.conf.urls import url
from django.contrib.auth.decorators import login_required, permission_required

from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    url(r'^problems/$', views.ProblemsClass.as_view()),
    url(r'^level/(?P<level>[0-9]+)/$', views.LevelDetails.as_view()),
    url(r'^problems/(?P<question_level>[0-9]+)/(?P<question_number>[0-9]+)/$', views.QuestionDetails.as_view()),
    url(r'^submit/(?P<question_level>[0-9]+)/(?P<question_number>[0-9]+)/$', views.SubmitAnswer.as_view()),
    url(r'^generate_scoreboard/$',views.Scoreboard.as_view()),
    url(r'^comment/(?P<question_level>[0-9]+)/(?P<question_number>[0-9]+)/$',views.PostComments.as_view()),
    url(r'^mysubmissions/$',views.MySubmissions.as_view()),
    url(r'^contest/$',views.ContestDetails.as_view()),
    url(r'^clarification/$',views.ClarificationDetails.as_view()),
    url(r'^startRejudgeFelicity/(?P<question_level>[0-9]+)/(?P<question_number>[0-9]+)/$',views.Rejudge.as_view()), #make sure not to give easy url which can be guessed
]

urlpatterns = format_suffix_patterns(urlpatterns)
