from rest_framework import serializers
from .models import Question, Submission, Profile, Comment, Contest, ClarificationMessages


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ('question_level', 'question_number', 'question_title', 'question_desc', 'question_image', 'question_enable_mathjax')

class SubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submission
        fields = ('id','submission_string','submission_storage')

class ScoreboardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('user_nick','user_score', 'user_country')

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('comment_message',)

class SubmissionSerializerforMySubmission(serializers.ModelSerializer):
    class Meta:
        model = Submission
        fields = ('submission_user',
                  'submission_question',
                  'submission_question',
                  'submission_state'
        )
        depth = 2

class ContestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contest
        fields = ('contest_name',
                  'contest_description',
                  'contest_rules',
                  'contest_start_time',
                  'contest_end_time',
        )

class ClarificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClarificationMessages
        fields = '__all__'

class QuestionCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'
