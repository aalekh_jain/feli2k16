# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import answerdiff.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AcceptedQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('score', models.FloatField(default=0.0)),
                ('tries', models.IntegerField(default=0)),
                ('accepted', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ClarificationMessages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('clarification_messages_message', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment_timestamp', models.DateTimeField(auto_now_add=True)),
                ('comment_message', models.CharField(default=b'', max_length=255)),
                ('comment_is_approved', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('contest_name', models.CharField(default=b'', unique=True, max_length=255)),
                ('contest_description', models.TextField(default=b'', blank=True)),
                ('contest_rules', models.TextField(default=b'')),
                ('contest_start_time', models.DateTimeField()),
                ('contest_end_time', models.DateTimeField()),
                ('contest_number_of_level', models.PositiveIntegerField()),
                ('contest_questions_in_each_level', models.PositiveIntegerField()),
                ('contest_user_level_increment_type', models.CharField(max_length=6, choices=[(b'TYPE1', b'TYPE1'), (b'TYPE2', b'TYPE2')])),
                ('contest_number_to_increment_level_at', models.PositiveIntegerField()),
                ('contest_question_type', models.CharField(max_length=6, choices=[(b'FL', b'File'), (b'ST', b'String')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language_compile_arguments', models.CharField(default=b'', max_length=255, blank=True)),
                ('language_runtime_arguments', models.CharField(default=b'', max_length=255, blank=True)),
                ('language_file_extension', models.CharField(default=b'', unique=True, max_length=16)),
                ('language_is_preprocessed', models.BooleanField(default=False)),
                ('language_is_sandboxed', models.BooleanField(default=False)),
                ('language_is_compiled', models.BooleanField(default=False)),
                ('language_is_checked', models.BooleanField(default=False)),
                ('language_is_executed', models.BooleanField(default=False)),
                ('language_name', models.CharField(default=b'', max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_nick', models.CharField(max_length=255)),
                ('user_country', models.CharField(max_length=255)),
                ('user_last_ip', models.GenericIPAddressField(default=b'0.0.0.0')),
                ('user_access_level', models.PositiveIntegerField(default=1)),
                ('user_score', models.FloatField(default=0)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question_level', models.IntegerField()),
                ('question_number', models.IntegerField()),
                ('question_title', models.CharField(unique=True, max_length=255)),
                ('question_desc', models.TextField()),
                ('question_image', models.ImageField(upload_to=answerdiff.models.question_image_filepath, blank=True)),
                ('question_upload_type', models.CharField(default=b'ST', max_length=2, choices=[(b'FL', b'File'), (b'ST', b'String')])),
                ('question_answer_string', models.CharField(default=b'', max_length=255, blank=True)),
                ('question_enable_mathjax', models.BooleanField(default=False)),
                ('question_upload_file', models.FileField(upload_to=answerdiff.models.question_input_file_upload, blank=True)),
                ('question_gold_upload_file', models.FileField(upload_to=answerdiff.models.question_gold_file_upload, blank=True)),
                ('question_checker_script', models.FileField(upload_to=answerdiff.models.question_checker_upload, blank=True)),
                ('question_preprocess_script', models.FileField(upload_to=answerdiff.models.question_preprocess_upload, blank=True)),
                ('question_time_limit', models.CharField(default=b'', max_length=16, blank=True)),
                ('question_memory_limit', models.CharField(default=b'', max_length=16, blank=True)),
                ('question_output_limit', models.CharField(default=b'', max_length=16, blank=True)),
                ('question_points', models.FloatField(default=100)),
                ('question_reduce_ratio', models.FloatField(default=1)),
                ('question_restrict_language_to', models.ForeignKey(default=None, blank=True, to='answerdiff.Language', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('submission_timestamp', models.DateTimeField(auto_now_add=True)),
                ('submission_string', models.CharField(default=b'', max_length=255, blank=True)),
                ('submission_storage', models.FileField(upload_to=answerdiff.models.submission_storage_path, blank=True)),
                ('submission_state', models.CharField(default=b'PR', max_length=2, choices=[(b'WA', b'Wrong Answer'), (b'AC', b'Accepted'), (b'PR', b'Processing')])),
                ('submission_score', models.FloatField(default=0)),
                ('submission_runtime_log', models.CharField(default=b'', max_length=255, blank=True)),
                ('submission_question', models.ForeignKey(to='answerdiff.Question')),
                ('submission_user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='comment',
            name='comment_question',
            field=models.ForeignKey(to='answerdiff.Question'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comment',
            name='comment_user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='acceptedquestion',
            name='record_question',
            field=models.ForeignKey(to='answerdiff.Question'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='acceptedquestion',
            name='record_user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
