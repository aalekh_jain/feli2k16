"""
Django settings for contest project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'fko%7t!2n#k0-s%a4+=f&ldjs=-#%+&-kpwlnl(og+-uz5#ouw'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True


ALLOWED_HOSTS = [
    '.felicity.iiit.ac.in',
    '.felicity.iiit.ac.in.',
]

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'answerdiff',
    'longerusername',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
        'answerdiff.backends.PopulatedCASBackend'
        )

ROOT_URLCONF = 'contest.urls'

WSGI_APPLICATION = 'contest.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'GordianKnot16',
        'NAME': 'GordianKnot16',
        'HOST': 'Shana',
        'PASSWORD': '5hw6ryIcwP4Y',
    }
}
# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

LOGGERS = { 
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'file': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': '/tmp/django-debug.log',
                },
            },
        'loggers': {
            'django': {
                'handlers': ['file'],
                'level': 'DEBUG',
                'propogate': True,
                },
            'answerdiff': {
                'handlers': ['file'],
                'level': 'ERROR',
                'propogate': True,
                },
            },
            'django_cas_ng': {
                'handlers': ['file'],
                'level': 'DEBUG',
                'propogate': True,
            },
        }

TEMPLATE_PATH = os.path.join(BASE_DIR, 'templates')
TEMPLATE_DIRS = (TEMPLATE_PATH,)

MEDIA_ROOT = os.path.abspath(os.path.dirname(__file__)) + '/media/'
MEDIA_URL = '/contest/gordian-knot/media/'
STATIC_ROOT = os.path.abspath(os.path.dirname(__file__)) + '/../static/'
STATIC_URL = '/contest/gordian-knot/static/'

BROKER_URL = 'redis://localhost:6379/0'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

CAS_SERVER_URL = 'https://felicity.iiit.ac.in/cas/'
CAS_VERSION = '3' 
CAS_LOGOUT_COMPLETLY = True
CAS_LOGOUT_URL = 'http://felicity.iiit.ac.in/auth/logout'
CAS_DISPLAY_MESSAGES = False
GIN_URL = '/contest/gordian-knot/accounts/login'
LOGOUT_URL = '/contest/gordian-knot/accounts/logout'
LOGIN_REDIRECT_URL = '/' 
CAS_FAILURE_URL = "https://felicity.iiit.ac.in/auth/login"

REST_FRAMEWORK = {
        'DEFAULT_RENDERER_CLASSES': (
            'rest_framework.renderers.JSONRenderer',
        )
    }
