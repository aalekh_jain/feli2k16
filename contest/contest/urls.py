from django.conf.urls import patterns, include, url
from django.contrib import admin
from answerdiff import views
from frontend.views import main

urlpatterns = [
    url(r'^contest/gordian-knot/admin/', include(admin.site.urls)),
    url(r'^contest/gordian-knot/accounts/login$', 'django_cas.views.login'),
    url(r'^contest/gordian-knot/accounts/logout$', 'django_cas.views.logout'),
    url(r'^contest/gordian-knot/api/', include('answerdiff.urls')),
    url(r'^contest/gordian-knot/', main),
]
